

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
```


```python
results = pd.DataFrame(columns=['items', 'count'])
for i in range(1, 8):
    # print('./output/{}/result.csv'.format((i + 1)/200))
    result = pd.read_csv('./output/{}/result.csv'.format((i + 1)/200))
    result['support'] = (i + 1) / 100
    results = results.append(result)
    plt.plot(result['items'], result['count'], label= str(i+1) + '%')
plt.legend()
plt.title('cnt / number of the itemset elemts')
plt.xlabel('number of items')
plt.ylabel('count')
```

    ./output/0.01/result.csv
    ./output/0.015/result.csv
    ./output/0.02/result.csv
    ./output/0.025/result.csv
    ./output/0.03/result.csv
    ./output/0.035/result.csv
    ./output/0.04/result.csv





    Text(0, 0.5, 'count')




![png](analyse_result_files/analyse_result_1_2.png)



```python
for i in range(4):
    this_results = results[results['items'] == i + 1]
    plt.plot(this_results['support'], this_results['count'], label= str(i+1))
plt.legend()
```




    <matplotlib.legend.Legend at 0x7f627756b1d0>




![png](analyse_result_files/analyse_result_2_1.png)



```python
times = []
for i in range(1, 8):
    f = open('./output/{}/note.txt'.format((i + 1)/200))
    data = f.read().split('m')
    data[1] = data[1].split('s')[0]
    print(int(data[0]) )
    times.append([(i + 1)/200, int(data[0])*60 + int(float(data[1]))])
times = pd.DataFrame(times, columns=['support', 'time'])
plt.plot(times['support'], times['time'])
plt.ylabel('time')
plt.xlabel('support')
plt.title('support vs time')
```

    3
    3
    3
    1
    1
    1
    1





    Text(0.5, 1.0, 'support vs time')




![png](analyse_result_files/analyse_result_3_2.png)

