import pandas as pd

import numpy as np

onlineOrders = pd.read_csv('./data/onlineOrder_cleaned.csv')
onlineOrders = onlineOrders.applymap(str)
for column in onlineOrders.columns:
    onlineOrders[column] = column + '_' + onlineOrders[column]
onlineOrders = onlineOrders.reindex(sorted(onlineOrders.columns), axis=1)

PERCENT = 0.01
MIN_SUPPORT = int(PERCENT * onlineOrders.count()[0])
print('MIN_SUPPORT : {}'.format(PERCENT))


def find_l1():
    c_1 = pd.DataFrame(columns=['item', 'cnt'])
    for column in onlineOrders.columns:
        c_temp = pd.DataFrame()
        c_temp['item'] = onlineOrders[column].value_counts().index
        c_temp['cnt'] = onlineOrders[column].value_counts().values
        c_temp['column'] = column
        c_1 = c_1.append(c_temp)
        l1 = c_1[c_1['cnt'] > MIN_SUPPORT][['item']]
        l1 = l1[list(map(lambda s: not s.endswith('_nan'), l1['item']))]
    return l1


def items_in_df2(items, df):
    if len(df.columns) != len(items):
        print('size error', len(df.columns), len(items))
    arr = None
    for i in range(len(df.columns)):
        if arr is None:
            arr = df[df.columns[i]] == items[i]
        else:
            arr = arr & (df[df.columns[i]] == items[i])
    return arr.sum()


def iterate(list_of_fp):
    if len(list_of_fp.columns[:-1]) > 0:
        joined = list_of_fp.merge(list_of_fp, on=list(list_of_fp.columns[:-1]))
        joined = joined[joined[joined.columns[-2]] < joined[joined.columns[-1]]]
    else:
        list_of_fp['key'] = 0
        joined = list_of_fp.merge(list_of_fp, on='key')
        joined = joined.drop('key', axis=1)
        list_of_fp = list_of_fp.drop('key', axis=1)
        joined = joined[joined[joined.columns[-2]] < joined[joined.columns[-1]]]
        joined.columns = ['A', 'B']
    accepted_items = []
    for index, row in joined.iterrows():
        flag = True
        for i in range(len(joined.columns)):
            items = list(row[:i]) + list(row[i + 1:])
            if items_in_df2(items, list_of_fp) == 0:
                flag = False
        accepted_items.append(flag)
    joined['accepted'] = np.array(accepted_items)

    accepted = joined[joined['accepted']]
    cnts = []
    for index, row in accepted.iterrows():
        columns_list = list(map(lambda x: x.split('_')[0], row[:-1]))
        if columns_list[0] == columns_list[1]:
            cnt = 0
        else:
            cnt = items_in_df2(row.values[:-1], onlineOrders[columns_list])
        cnts.append(cnt)
    accepted['cnt'] = np.array(cnts)
    l2_final = accepted[accepted['cnt'] >= MIN_SUPPORT]
    l2_final = l2_final.drop(['accepted', 'cnt'], axis=1, errors='ignore')
    return l2_final


l1 = find_l1()
ls = [l1]
print(1, ls[-1].count()[0])

for i in range(len(onlineOrders.columns)):
    ls.append(iterate(ls[-1]))
    print(i + 2, ls[-1].count()[0])
    if ls[-1].count()[0] == 0:
        break

# 45 52 14 ...
