

```python
import pandas as pd

import numpy as np

import itertools
import os
pd.options.mode.chained_assignment = None
MIN_SUPPORT_PERCENT = 0.015


```


```python
onlineOrders = pd.read_csv('./data/onlineOrder_cleaned.csv')
onlineOrders = onlineOrders.applymap(str)
```


```python
stocks_list = sorted(list(onlineOrders[['StockCode']].values[:,0]))
```


```python
unique_stocks_list = []
for stock in stocks_list:
    if len(unique_stocks_list) == 0 or stock != unique_stocks_list[-1]:
        unique_stocks_list.append(stock)
print('number of stocks:', len(unique_stocks_list))
```

    number of stocks: 4058



```python
grouped_items = onlineOrders.groupby('InvoiceNo')['StockCode']
items = []
for item, method in grouped_items:
    items.append(sorted(list(method)))
all_df = pd.DataFrame(items)

```


```python
cnt = 0
boolean_arrs_list = []
for index, row in all_df.iterrows():
    # print(row)
    cnt += 1
    if cnt % 300 == 0:
        print('\r {} %'.format(int(cnt * 100/ len(all_df))), end='')
    items = []
    boolean_arr = []
    for item in row:
        if item is not None:
            items.append(item)
    for column in unique_stocks_list:
        if column in items:
            boolean_arr.append(True)
        else:
            boolean_arr.append(False)            

    boolean_arrs_list.append(boolean_arr)
big_df = pd.DataFrame(boolean_arrs_list, columns=unique_stocks_list)

```

     99 %


```python
# MIN_SUPPORT_PERCENT = 0.02
# print('\rMin Support {}%'.format(MIN_SUPPORT_PERCENT))
# MIN_SUPPORT = int(MIN_SUPPORT_PERCENT * all_df.count()[0])
# print('\rMin Support {}'.format(MIN_SUPPORT))

```


```python
number = 3
frequent_patterns = pd.read_csv('./output/0.01/l_{}.csv'.format(number))
frequent_patterns = frequent_patterns.applymap(str)
```


```python
def split_X_Y(sel_index, items):
    X = items[sel_index]
    Y = [x for x in items if x != X]
    return X, Y
sel_index = 0
print(split_X_Y(sel_index, items))
```

    ('20711', ['20712', '85099B'])



```python
def rows_contains_items(dataframe, items):
    sel_row = np.alltrue(len(dataframe))
    for column in items:
        sel_row = sel_row & dataframe[column]
    contains_items_df = dataframe[sel_row]
    return contains_items_df

len(rows_contains_items(big_df, items))
```




    227




```python
contains_x_df = rows_contains_items(big_df, [X])
len(contains_x_df)
```




    525




```python
len(big_df)
```




    22061




```python
# print('support: {}, confident: {}'.format(len(contains_items_df) / len(big_df), len(contains_items_df) / len(contains_x_df)))
```

    support: 0.010289651421059788, confident: 0.43238095238095237



```python
stats = []
for index, item in frequent_patterns.iterrows():
    for i in range(len(frequent_patterns.columns)):
        X, Y = split_X_Y(i, item)
        have_x = rows_contains_items(big_df, [X])
        have_y = rows_contains_items(big_df, Y)
        have_both = rows_contains_items(big_df, [X]+Y)
        support = len(have_both) / len(big_df)
        confident = len(have_both) / len(have_x)
        lift = (len(have_both) / (len(have_x) * len(have_y))) * len(big_df)
        stats.append([X, Y, support, confident, lift])
stats = pd.DataFrame(stats, columns=['X', 'Y', 'support', 'confident', 'lift'])
```


```python
stats['all_confidence'] = stats['support'] / max(stats['support'])
```


```python
stats[(stats['confident'] > 0.8) & (stats['support'] > 0.02)].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X</th>
      <th>Y</th>
      <th>support</th>
      <th>confident</th>
      <th>lift</th>
      <th>all_confidence</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1013</th>
      <td>22698</td>
      <td>[22697]</td>
      <td>0.028693</td>
      <td>0.825293</td>
      <td>17.937731</td>
      <td>0.767273</td>
    </tr>
  </tbody>
</table>
</div>




```python
stats[(stats['confident'] > 0.6) & (stats['support'] > 0.02)].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X</th>
      <th>Y</th>
      <th>support</th>
      <th>confident</th>
      <th>lift</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>451</th>
      <td>22698</td>
      <td>[22697, 22699]</td>
      <td>0.024568</td>
      <td>0.706649</td>
      <td>20.298685</td>
    </tr>
  </tbody>
</table>
</div>




```python
stats[(stats['confident'] > 0.4) & (stats['support'] > 0.02)].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X</th>
      <th>Y</th>
      <th>support</th>
      <th>confident</th>
      <th>lift</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>450</th>
      <td>22697</td>
      <td>[22698, 22699]</td>
      <td>0.024568</td>
      <td>0.533990</td>
      <td>19.666706</td>
    </tr>
    <tr>
      <th>451</th>
      <td>22698</td>
      <td>[22697, 22699]</td>
      <td>0.024568</td>
      <td>0.706649</td>
      <td>20.298685</td>
    </tr>
    <tr>
      <th>452</th>
      <td>22699</td>
      <td>[22697, 22698]</td>
      <td>0.024568</td>
      <td>0.508443</td>
      <td>17.719994</td>
    </tr>
  </tbody>
</table>
</div>




```python
stats[(stats['lift'] > 50)].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X</th>
      <th>Y</th>
      <th>support</th>
      <th>confident</th>
      <th>lift</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>472</th>
      <td>23171</td>
      <td>[23170, 23172]</td>
      <td>0.011559</td>
      <td>0.669291</td>
      <td>54.889354</td>
    </tr>
    <tr>
      <th>473</th>
      <td>23172</td>
      <td>[23170, 23171]</td>
      <td>0.011559</td>
      <td>0.836066</td>
      <td>57.819569</td>
    </tr>
  </tbody>
</table>
</div>




```python
stats[(stats['confident'] > 0.70)].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>X</th>
      <th>Y</th>
      <th>support</th>
      <th>confident</th>
      <th>lift</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>451</th>
      <td>22698</td>
      <td>[22697, 22699]</td>
      <td>0.024568</td>
      <td>0.706649</td>
      <td>20.298685</td>
    </tr>
    <tr>
      <th>473</th>
      <td>23172</td>
      <td>[23170, 23171]</td>
      <td>0.011559</td>
      <td>0.836066</td>
      <td>57.819569</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.pyplot as plt
%matplotlib inline
plt.scatter(stats['confident'], stats['lift'])

```




    <matplotlib.collections.PathCollection at 0x7f1d6290bac8>




![png](mining_freq_correct_clean_association_files/mining_freq_correct_clean_association_20_1.png)



```python
plt.scatter(stats['lift'], stats['support'])

```




    <matplotlib.collections.PathCollection at 0x7f1d52d944a8>




![png](mining_freq_correct_clean_association_files/mining_freq_correct_clean_association_21_1.png)



```python
plt.scatter(stats['confident'], stats['support'])

```




    <matplotlib.collections.PathCollection at 0x7f1d62260438>




![png](mining_freq_correct_clean_association_files/mining_freq_correct_clean_association_22_1.png)

