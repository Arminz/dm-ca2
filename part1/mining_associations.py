
# coding: utf-8

# In[1]:


import pandas as pd

import numpy as np

import itertools
import os
pd.options.mode.chained_assignment = None
MIN_SUPPORT_PERCENT = 0.015
MIN_CONFIDENT_PERCENT = 0.4


# In[2]:


onlineOrders = pd.read_csv('./data/onlineOrder_cleaned.csv')
onlineOrders = onlineOrders.applymap(str)


# In[3]:


stocks_list = sorted(list(onlineOrders[['StockCode']].values[:,0]))


# In[4]:


unique_stocks_list = []
for stock in stocks_list:
    if len(unique_stocks_list) == 0 or stock != unique_stocks_list[-1]:
        unique_stocks_list.append(stock)
print('number of stocks:', len(unique_stocks_list))


# In[5]:


grouped_items = onlineOrders.groupby('InvoiceNo')['StockCode']
items = []
for item, method in grouped_items:
    items.append(sorted(list(method)))
all_df = pd.DataFrame(items)


# In[7]:


cnt = 0
boolean_arrs_list = []
for index, row in all_df.iterrows():
    # print(row)
    cnt += 1
    if cnt % 300 == 0:
        print('\r {} %'.format(int(cnt * 100/ len(all_df))), end='')
    items = []
    boolean_arr = []
    for item in row:
        if item is not None:
            items.append(item)
    for column in unique_stocks_list:
        if column in items:
            boolean_arr.append(True)
        else:
            boolean_arr.append(False)            

    boolean_arrs_list.append(boolean_arr)
big_df = pd.DataFrame(boolean_arrs_list, columns=unique_stocks_list)


# In[9]:


number = 3
frequent_patterns = pd.read_csv('./output/0.01/l_{}.csv'.format(number))
frequent_patterns = frequent_patterns.applymap(str)


# In[11]:


def split_X_Y(sel_index, items):
    X = items[sel_index]
    Y = [x for x in items if x != X]
    return X, Y
sel_index = 0
print(split_X_Y(sel_index, items))


# In[13]:


def rows_contains_items(dataframe, items):
    sel_row = np.alltrue(len(dataframe))
    for column in items:
        sel_row = sel_row & dataframe[column]
    contains_items_df = dataframe[sel_row]
    return contains_items_df

len(rows_contains_items(big_df, items))


# In[79]:



# In[69]:


# In[70]:


# print('support: {}, confident: {}'.format(len(contains_items_df) / len(big_df), len(contains_items_df) / len(contains_x_df)))


# In[15]:


stats = []
for index, item in frequent_patterns.iterrows():
    for i in range(len(frequent_patterns.columns)):
        X, Y = split_X_Y(i, item)
        have_x = rows_contains_items(big_df, [X])
        have_y = rows_contains_items(big_df, Y)
        have_both = rows_contains_items(big_df, [X]+Y)
        support = len(have_both) / len(big_df)
        confident = len(have_both) / len(have_x)
        lift = (len(have_both) / (len(have_x) * len(have_y))) * len(big_df)
        stats.append([X, Y, support, confident, lift])
stats = pd.DataFrame(stats, columns=['X', 'Y', 'support', 'confident', 'lift'])
stats = stats[stats['confident'] > MIN_CONFIDENT_PERCENT]
outdir = './output/association_rules/{}_{}'.format(number, MIN_CONFIDENT_PERCENT)
if not os.path.exists(outdir):
    os.mkdir(outdir)
stats.to_csv(outdir + '/associations_result.csv', index=False)