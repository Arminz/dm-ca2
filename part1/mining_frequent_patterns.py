# coding: utf-8

# In[1]:


import pandas as pd

import numpy as np

import itertools
import os
pd.options.mode.chained_assignment = None
MIN_SUPPORT_PERCENT = 0.015
# In[2]:


onlineOrders = pd.read_csv('./data/onlineOrder_cleaned.csv')
onlineOrders = onlineOrders.applymap(str)

# In[5]:


stocks_list = sorted(list(onlineOrders[['StockCode']].values[:, 0]))

# In[6]:


unique_stocks_list = []
for stock in stocks_list:
    if len(unique_stocks_list) == 0 or stock != unique_stocks_list[-1]:
        unique_stocks_list.append(stock)
print('number of stocks:', len(unique_stocks_list))

# In[7]:


grouped_items = onlineOrders.groupby('InvoiceNo')['StockCode']
items = []
for item, method in grouped_items:
    items.append(sorted(list(method)))
all_df = pd.DataFrame(items)

# In[59]:


cnt = 0
boolean_arrs_list = []
for index, row in all_df.iterrows():
    # print(row)
    cnt += 1

    items = []
    boolean_arr = []
    for item in row:
        if item is not None:
            items.append(item)
    for column in unique_stocks_list:
        if column in items:
            boolean_arr.append(True)
        else:
            boolean_arr.append(False)
    boolean_arrs_list.append(boolean_arr)
    if cnt % 300 == 0:
        print('\r{} %'.format(int(cnt * 100 / len(all_df)) + 1), end='')
big_df = pd.DataFrame(boolean_arrs_list, columns=unique_stocks_list)

# In[38]:


print('\rMin Support {}%'.format(MIN_SUPPORT_PERCENT))
MIN_SUPPORT = int(MIN_SUPPORT_PERCENT * all_df.count()[0])

# In[69]:


l_1 = []
for column in big_df.columns:
    if sum(big_df[column]) > MIN_SUPPORT:
        l_1.append(column)
l_1 = pd.DataFrame(l_1, columns=['item'])
l_1.head()
print(len(l_1))


# In[63]:


def items_in_df2(items, df):
    if len(df.columns) != len(items):
        print('size error', len(df.columns), len(items))
    arr = None
    for i in range(len(df.columns)):
        if arr is None:
            arr = (df[df.columns[i]] == items[i])
        else:
            arr = arr & (df[df.columns[i]] == items[i])

    return arr.sum()


# In[71]:


def iterate(list_of_fp):
    # print('len(list_of_fp', len(list_of_fp))
    if len(list_of_fp.columns[:-1]) > 0:
        joined = list_of_fp.merge(list_of_fp, on=list(list_of_fp.columns[:-1]))
        joined = joined[joined[joined.columns[-2]] < joined[joined.columns[-1]]]
    else:
        list_of_fp['key'] = 0
        joined = list_of_fp.merge(list_of_fp, on='key')
        joined = joined.drop('key', axis=1)
        list_of_fp = list_of_fp.drop('key', axis=1)
        joined = joined[joined[joined.columns[-2]] < joined[joined.columns[-1]]]
        joined.columns = ['A', 'B']
    print('l_i joined to itself')
    print('pruning...')
    accepted_items = []
    for index, row in joined.iterrows():
        flag = True
        for i in range(len(joined.columns)):
            items = list(row[:i]) + list(row[i + 1:])
            if items_in_df2(items, list_of_fp) == 0:
                flag = False
        accepted_items.append(flag)
    joined['accepted'] = np.array(accepted_items)

    accepted = joined[joined['accepted']]

    print('candidates generated, count: {}'.format(len(accepted)))
    print('counting based on db...'.format(len(accepted)))
    progress = 0
    cnts = []
    for index, row in accepted.iterrows():
        progress += 1
        if progress % 1000 == 0:
            print('\r{} %'.format(int(progress * 100 / len(accepted))), end='')
        columns = row[:-1].values
        sel_row = np.alltrue(len(big_df))
        for column in columns:
            sel_row = big_df[column] & sel_row
        cnt = sel_row.sum()
        cnts.append(cnt)
    accepted.loc[:,'cnt'] = np.array(cnts)
    # accepted['cnt'] = np.array(cnts)
    l2_final = accepted[accepted['cnt'] >= MIN_SUPPORT]
    l2_final = l2_final.drop(['accepted', 'cnt'], axis=1, errors='ignore')
    return l2_final


# In[73]:

l_1 = l_1.drop('key', axis=1, errors='ignore')
l_1 = l_1.applymap(str)
l_i = [l_1]
results = []
i = 0
outdir = './output/{}'.format(MIN_SUPPORT_PERCENT)
while len(l_i[-1]) > 0:
    print('\rnumber of items : {} -> number of frequent itemsets : {}'.format(i + 1, len(l_i[-1])))
    results.append([i+1, len(l_i[-1])])
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    l_i[-1].to_csv('{}/l_{}.csv'.format(outdir, i+1), index=False)
    l_n = iterate(l_i[-1])
    l_i.append(l_n)
    i += 1

result_df = pd.DataFrame(results, columns=['items', 'count'])
result_df.to_csv('{}/result.csv'.format(outdir), index=False)
