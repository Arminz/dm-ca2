
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


df = pd.read_excel('./data/onlineOrder.xlsx')
print(df.head(2))


# In[4]:


# df['InvoiceNo']#.apply(lambda x: x[0])
print(df[df['Description'] == 'Discount'].head())


# In[14]:


cleaned_data = df[df['InvoiceNo'].apply(lambda x: str(x)[0] not in ['A', 'C'])]


# In[17]:


cleaned_data.to_csv('./data/onlineOrder_cleaned.csv', index=False)

