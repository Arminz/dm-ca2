
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


data = pd.read_csv('./data/onlineOrder_cleaned.csv')


# In[3]:


cleaned_data = data[~data['CustomerID'].isna()]


# In[4]:


len(cleaned_data) / len(data)


# In[5]:


print(cleaned_data.head())


# In[6]:


grouped_items = cleaned_data.groupby('CustomerID')['StockCode', 'InvoiceDate', 'InvoiceNo']


# In[36]:


customer_ids = []
customer_sequences = []
for customerID, items in grouped_items:
    customer_sequence = []
    transaction = []
    last_time = 0
    for item in items.values:
        if len(transaction) == 0 or last_time == item[4]:
            transaction.append(list(item)[1])
            last_time = item[4]
        else:
            customer_sequence.append(transaction)
            transaction = [list(item)[1]]
            last_time = item[4]
        customer_sequence.append(transaction)
    customer_sequences.append(str(customer_sequence))
    customer_ids.append(customerID)


# In[43]:


customer_sequence_df = pd.DataFrame(customer_ids, customer_sequences)


# In[52]:




